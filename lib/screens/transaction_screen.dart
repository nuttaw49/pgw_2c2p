import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pgw_2c2p/controllers/transaction_controller.dart';
import 'package:url_launcher/url_launcher.dart';

class TransactionScreen extends StatelessWidget {
  @override
  Widget build(context) {
    final TransactionController _transactionController =
        Get.put(TransactionController());
    _transactionController.setHash();

    return Scaffold(
      appBar: AppBar(title: Text("2C2P Transaction")),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20),
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                    labelText: "Url 2C2P",
                    hintText: "Url 2C2P",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.url2c2p,
                  keyboardType: TextInputType.text,
                  readOnly: true,
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Secret Key",
                    hintText: "Secret Key",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.secretKey,
                  keyboardType: TextInputType.text,
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Version",
                    hintText: "Version",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.version,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Timestamp",
                    hintText: "TimeStamp",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.timeStemp,
                  keyboardType: TextInputType.text,
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "merchant id",
                    hintText: "merchant id",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.mid,
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Order Prefix",
                    hintText: "Order Prefix",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.orderIdPrefix,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Description",
                    hintText: "Description",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.description,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Currency",
                    hintText: "Currency",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.currency,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Amount",
                    hintText: "Amount",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.amount,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Allow Multiple Payment",
                    hintText: "Allow Multiple Payment",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.allowMultiplePayment,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Expiry",
                    hintText: "Expiry",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.expiry,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Result Url 1",
                    hintText: "Result Url 1",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.resultUrl1,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Result Url 2",
                    hintText: "Result Url 2",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.resultUrl2,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _transactionController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "String To Hash",
                    hintText: "String To Hash",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.stringToHash,
                  keyboardType: TextInputType.text,
                  maxLines: 3,
                  readOnly: true,
                ),
                SizedBox(height: 30),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Hash Data",
                    hintText: "Hash Data",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.hash,
                  keyboardType: TextInputType.text,
                  readOnly: true,
                ),
                SizedBox(height: 30),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Json",
                    hintText: "Json",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.reqJson,
                  keyboardType: TextInputType.text,
                  maxLines: 5,
                  readOnly: true,
                ),
                SizedBox(height: 30),
                TextField(
                  decoration: InputDecoration(
                    labelText: "base64",
                    hintText: "base64",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _transactionController.base64String,
                  keyboardType: TextInputType.text,
                  maxLines: 5,
                  readOnly: true,
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {
                          _transactionController.createTransaction();
                        },
                        child: Text('Create'),
                      ),
                    )
                  ],
                ),
                Obx(
                  () => Visibility(
                    visible: _transactionController.responseVisible.value,
                    child: Column(
                      children: [
                        SizedBox(height: 10),
                        SizedBox(height: 30),
                        TextField(
                          decoration: InputDecoration(
                            labelText: "Response Data",
                            hintText: "Response Data",
                            filled: true,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          controller: _transactionController.response,
                          keyboardType: TextInputType.text,
                          maxLines: 5,
                          readOnly: true,
                        ),
                        SizedBox(height: 10),
                        TextField(
                          decoration: InputDecoration(
                            labelText: "Response Decoded Data",
                            hintText: "Response Decoded Data",
                            filled: true,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          controller: _transactionController.responseDecode,
                          keyboardType: TextInputType.text,
                          maxLines: 5,
                          readOnly: true,
                        ),
                        SizedBox(height: 10),
                        Divider(height: 5),
                        SizedBox(height: 10),
                        Obx(
                          () => Visibility(
                            visible:
                                (_transactionController.quickPayUrl.value != "")
                                    ? true
                                    : false,
                            child: Column(
                              children: [
                                RichText(
                                  text: TextSpan(
                                    style: TextStyle(color: Colors.blue),
                                    text: "Click to Pay",
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () async {
                                        var url = _transactionController
                                            .quickPayUrl.value;
                                        if (await canLaunch(url)) {
                                          await launch(
                                            url,
                                            forceSafariVC: false,
                                          );
                                        }
                                      },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
