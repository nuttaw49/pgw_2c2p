import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pgw_2c2p/controllers/inquiry_controller.dart';

class InquiryScreen extends StatelessWidget {
  @override
  Widget build(context) {
    final InquiryController _inquiryController = Get.put(InquiryController());
    _inquiryController.setHash();

    return Scaffold(
      appBar: AppBar(title: Text("2C2P Inquiry")),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20),
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                    labelText: "Url",
                    hintText: "Url",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.url2c2p,
                  keyboardType: TextInputType.text,
                  readOnly: true,
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Version",
                    hintText: "Version",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.version,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _inquiryController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Secret Key",
                    hintText: "Secret Key",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.secretKey,
                  keyboardType: TextInputType.text,
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "merchant id",
                    hintText: "merchant id",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.mid,
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    _inquiryController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Invoice No",
                    hintText: "Invoice No",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.invoiceNo,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    _inquiryController.setHash();
                  },
                ),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                    labelText: "String To Hash",
                    hintText: "String To Hash",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.stringToHash,
                  keyboardType: TextInputType.text,
                  maxLines: 1,
                  readOnly: true,
                ),
                SizedBox(height: 30),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Hash Data",
                    hintText: "Hash Data",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.hash,
                  keyboardType: TextInputType.text,
                  readOnly: true,
                ),
                SizedBox(height: 30),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Xml",
                    hintText: "Xml",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.xml,
                  keyboardType: TextInputType.text,
                  maxLines: 5,
                  readOnly: true,
                ),
                SizedBox(height: 30),
                TextField(
                  decoration: InputDecoration(
                    labelText: "base64",
                    hintText: "base64",
                    filled: true,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  controller: _inquiryController.base64String,
                  keyboardType: TextInputType.text,
                  maxLines: 5,
                  readOnly: true,
                ),
                // SizedBox(height: 10),
                // Row(
                //   children: [
                //     Expanded(
                //       child: ElevatedButton(
                //         onPressed: () {
                //           _inquiryController.inquiryTransaction();
                //         },
                //         child: Text('Inquiry'),
                //       ),
                //     )
                //   ],
                // ),
                // SizedBox(height: 30),
                // TextField(
                //   decoration: InputDecoration(
                //     labelText: "Response Data",
                //     hintText: "Response Data",
                //     filled: true,
                //     fillColor: Colors.black12,
                //     border: OutlineInputBorder(
                //       borderRadius: BorderRadius.circular(10),
                //     ),
                //   ),
                //   controller: _inquiryController.response,
                //   keyboardType: TextInputType.text,
                //   maxLines: 5,
                //   readOnly: true,
                // ),
                // SizedBox(height: 30),
                // TextField(
                //   decoration: InputDecoration(
                //     labelText: "Response Decoded Data",
                //     hintText: "Response Decoded Data",
                //     filled: true,
                //     fillColor: Colors.black12,
                //     border: OutlineInputBorder(
                //       borderRadius: BorderRadius.circular(10),
                //     ),
                //   ),
                //   controller: _inquiryController.responseDecode,
                //   keyboardType: TextInputType.text,
                //   maxLines: 5,
                //   readOnly: true,
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
