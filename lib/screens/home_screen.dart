import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'inquiry_screen.dart';
import 'transaction_screen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Screen"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.center, 
            children: [
              Container(
                width: 200,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(padding: EdgeInsets.all(20)),
                  onPressed: () {
                    Get.to(
                      InquiryScreen(),
                    );
                  },
                  child: Text('Inquiry Screen'),
                ),
              )
            ]),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 200,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(padding: EdgeInsets.all(20)),
                    onPressed: () {
                      Get.to(TransactionScreen());
                    },
                    child: Text('Transaction Screen'),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10)
          ],
        ),
      ),
    );
  }
}
