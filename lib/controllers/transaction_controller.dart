import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

class TransactionController extends GetxController {
  final url2c2p = TextEditingController();
  final secretKey = TextEditingController();

  final version = TextEditingController();
  final timeStemp = TextEditingController();
  final mid = TextEditingController();
  final orderIdPrefix = TextEditingController();
  final description = TextEditingController();
  final currency = TextEditingController();
  final amount = TextEditingController();
  final allowMultiplePayment = TextEditingController();
  final expiry = TextEditingController();

  final resultUrl1 = TextEditingController();
  final resultUrl2 = TextEditingController();

  final stringToHash = TextEditingController();
  final hash = TextEditingController();
  final reqJson = TextEditingController();
  final base64String = TextEditingController();

  // response
  final response = TextEditingController();
  final responseDecode = TextEditingController();
  var quickPayUrl = "".obs;
  var responseVisible = false.obs;

  TransactionController() {
    initializeDateFormatting();
    url2c2p.text = "https://demo2.2C2P.com/2C2PFrontEnd/quickPay/directApi";
    version.text = "2.1";
    secretKey.text =
        "F97DF81F947A53C291DCA84506BB7EC09797C2732652D5D62CD6F15625857992";
    mid.text = "014050000000006";
    orderIdPrefix.text = "PLAAA00019";
    description.text = "NIKE AIR MAX 2021 Blue";
    currency.text = "THB";
    amount.text = "2500.00";
    allowMultiplePayment.text = "N";
    expiry.text =
        DateFormat("ddMMyyyy").format(DateTime.now().add(Duration(days: 30)));

    var format = DateFormat("yyyyMMddhhmmss");
    var dateString = format.format(DateTime.now());
    timeStemp.text = dateString;

    resultUrl1.text = "http://test2c2ppgw.atwebpages.com/server.php";
    resultUrl2.text = "http://test2c2ppgw.atwebpages.com/server.php";
    

    orderIdPrefix.text = dateString;

  }

  setHash() {
    var key = utf8.encode(secretKey.text);
    var stringHash = getStringToHash();
    var bytes = utf8.encode(stringHash);
    var hmacSha1 = new Hmac(sha1, key);
    Digest sha1Result = hmacSha1.convert(bytes);
    hash.text = sha1Result.toString();
    stringToHash.text = stringHash;

    reqJson.text = '''
    {
      "GenerateQPReq":
      {
        "version":"${version.text}",
        "timeStamp":"${timeStemp.text}",
        "merchantID":"${mid.text}",
        "orderIdPrefix":"${orderIdPrefix.text}",
        "description":"${description.text}",
        "currency":"${currency.text}",
        "amount":"${amount.text}",
        "allowMultiplePayment":"${allowMultiplePayment.text}",
        "expiry":"${expiry.text}",
        "resultUrl1":"${resultUrl1.text}",
        "resultUrl2":"${resultUrl2.text}",
        "hashValue":"${hash.text}"
      }
    }
    ''';

    var str = reqJson.text;
    var stringToBase64 = utf8.encode(str);
    var base64Str = base64.encode(stringToBase64);

    base64String.text = base64Str;
  }

  String getStringToHash() {
    return version.text +
        timeStemp.text +
        mid.text +
        orderIdPrefix.text +
        description.text +
        currency.text +
        amount.text +
        allowMultiplePayment.text +
        expiry.text +
        resultUrl1.text +
        resultUrl2.text;
  }

  createTransaction() async {
    var url = Uri.parse("${url2c2p.text}");
    var response = await http.post(
      url,
      body: '${base64String.text}',
    );

    this.response.text = response.body;
    var base64Decoded = utf8.decode(base64.decode(response.body));
    this.responseDecode.text = base64Decoded;

    // String base64Decoded =
    //     '''{"GenerateQPRes":{"version":"2.1","timeStamp":"20210728181506","qpID":"482884","orderIdPrefix":"PLAAA00021","currency":"THB","amount":"12000.00","expiry":"27082021","url":"https://demo2.2c2p.com/2C2PFrontend/qp/EQJ4","resCode":"000","resDesc":"Success","hashValue":"E5325F129E82241BDC247EB0FA38A2427BFBD9E0"}}''';

    final Map<String, dynamic> data = json.decode(base64Decoded);

    if (base64Decoded != ""){
      responseVisible.value = true;
    }
    else{
      responseVisible.value = false;
    }

    if (data["GenerateQPRes"]["url"] != "") {
      quickPayUrl.value = data["GenerateQPRes"]["url"];
    }
    
  }
}
