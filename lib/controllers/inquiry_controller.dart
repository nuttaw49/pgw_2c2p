import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

import 'package:http/http.dart' as http;

class InquiryController extends GetxController {
  final url2c2p = TextEditingController();
  final version = TextEditingController();
  final secretKey = TextEditingController();
  final mid = TextEditingController();
  final invoiceNo = TextEditingController();
  final processType = TextEditingController();

  final stringToHash = TextEditingController();
  final hash = TextEditingController();
  final xml = TextEditingController();
  final base64String = TextEditingController();

  // response
  final response = TextEditingController();
  final responseDecode = TextEditingController();

  InquiryController() {
    initializeDateFormatting();

    url2c2p.text =
        "https://demo2.2c2p.com/2C2PFrontend/PaymentActionV2/PaymentProcess.aspx";
         
    version.text = "3.4";
    secretKey.text =
        "F97DF81F947A53C291DCA84506BB7EC09797C2732652D5D62CD6F15625857992";
    mid.text = "014050000000006";

    var format = DateFormat("yyyyMMddhhmmss");
    var dateString = format.format(DateTime.now());
    invoiceNo.text = dateString;
    processType.text = "I";
  }

  setHash() {
    var key = utf8.encode(secretKey.text);
    var _stringHash = getStringToHash();
    var bytes = utf8.encode(_stringHash);
    var hmacSha1 = new Hmac(sha1, key);
    Digest sha1Result = hmacSha1.convert(bytes);
    hash.text = sha1Result.toString();
    stringToHash.text = _stringHash;

    xml.text = '''<PaymentProcessRequest>
    <version>${version.text}</version>
    <merchantID>${mid.text}</merchantID>
    <processType>${processType.text}</processType>
    <invoiceNo>${invoiceNo.text}</invoiceNo>
    <hashValue>${hash.text}</hashValue>
    </PaymentProcessRequest>''';

    var str = xml.text;
    var stringToBase64 = utf8.encode(str);
    var base64Str = base64.encode(stringToBase64);

    base64String.text = base64Str;
  }

  String getStringToHash() {
    return version.text + mid.text + processType.text + invoiceNo.text;
  }

  inquiryTransaction() async {
    var url = Uri.parse("https://demo2.2C2P.com/2C2PFrontEnd/quickPay/directApi");
    //var url = Uri.parse("${url2c2p.text}");

    //print('Url: ${url2c2p.text}');

    var resp = await http.post(url);


    //var resp = await Dio().post('https://demo2.2c2p.com/2C2PFrontend/PaymentActionV2/PaymentProcess.aspx');
    print('Response body: ${resp.statusCode}');
    print('Response body: ${resp.body}');
    // var formData = dios.FormData.fromMap({
    //   'paymentRequest': base64String.text 
    // });
    // var response = await dio.post('${url2c2p.text}', data: formData);

    // var data = {'paymentRequest': '${base64String.text}'};
    // HttpRequest.postFormData('${url2c2p.text}', data).then((HttpRequest resp) {
    //   // Do something with the response.
    //   print('Response status: ${resp.status}');
    //   print('Response body: ${resp.responseText}');
    // });

    //var map = new Map<String, String>();
    //map["paymentRequest"] = base64String.text;

    // var response = await http.post(
    //   url,
    //   body: "paymentRequest=${base64String.text}",
    // );

    //var request = http.MultipartRequest('POST', url);
    //request.fields["paymentRequest"] = base64String.text;

    //request.send();

    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    //this.response.text = await response.stream.bytesToString();

    //var base64Decoded = utf8.decode(base64.decode(this.response.text));
    //this.responseDecode.text = base64Decoded;
  }
}
