import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pgw_2c2p/screens/home_screen.dart';


void main() => runApp(
      GetMaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    );




